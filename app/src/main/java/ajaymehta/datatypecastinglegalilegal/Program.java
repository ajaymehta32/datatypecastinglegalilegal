package ajaymehta.datatypecastinglegalilegal;

/**
 * Created by Avi Hacker on 7/14/2017.
 */

public class Program {

    //3. Which of the following are legal lines of Java code?
    public static void seeCasting(){

        // In all other cases (byte, short, char), you need the cast as there is no specific suffix.

        int a = (int) 888.8;  // when taking value in points. u dont define f or d ..it automatically take double
        byte b = (byte) 100L;  // l or L stands for long value  // casting long to byte
        long x = 100l;  // small  l
        double y = 2.3d;
        float f = 2.3f;

        // here is the thing ..you can put almost any data type value in anyother datavalue ..like in int,short,char,long,byte
        // so cases that you cant do like float,Stirng,double for  int kind type of value..

        byte m = (short) 50;   // putting short in byte
        short m2 = (byte) 50;

        long s = (byte)100;  // we can put byte value in long data type // byte value is noting just like int with 10^2
        int s2 = (byte)100;  // i can put byte value in int...

        long k = (int) 500L;
        int k2 =(char) 500L;


        // TODO ..uncomment n see what you cant do..
        //    int k4 =(float) 500L;
        // int k5 =(String) 500L;
        //   int k6 =(double) 500L;



        System.out.println(a);
        System.out.println(b);
        System.out.println(x);
        System.out.println(y);
        System.out.println(f);
        System.out.println(s);

        System.out.println(m);
        System.out.println(m2);
        System.out.println(s);
        System.out.println(s2);
        System.out.println(k);
        System.out.println(k2);


    }

    public static void main(String args[]) {

        seeCasting();

    }
}
